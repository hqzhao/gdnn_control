#ifndef DIFFERENTIAL_DRIVE_SYSTEM_H
#define DIFFERENTIAL_DRIVE_SYSTEM_H

#include <stdint.h>
#include <interrupt.h>

class CDifferentialDriveSystem {
public:
   CDifferentialDriveSystem();

   void SetTargetVelocity(int16_t velocity, int16_t time);
    void SetPIDParams(float f_Kp, float f_Ki, float f_Kd);

    int16_t GetVelocity();
    int16_t GetTargetVelocity();
    int16_t GetErrorListMember(int16_t num);
    int16_t GetAveragedError();
    int16_t GetErrorListLgh();
    void GetCopyErrorList(int16_t errorL[]);
    int16_t GetLastError();
    int16_t GetErrorDeriv();
    int32_t GetErrorInt();
   void Enable();
   void Disable();

public:
   enum class EBridgeMode {
      COAST,
      REVERSE,
      REVERSE_PWM_FD,
      REVERSE_PWM_SD,
      FORWARD,
      FORWARD_PWM_FD,
      FORWARD_PWM_SD,
      BRAKE
   };

   void ConfigureLeftMotor(EBridgeMode e_mode, uint8_t un_duty_cycle = 0);

   void ConfigureMotor(EBridgeMode e_mode, uint8_t un_duty_cycle = 0);
   void ConfigureRightMotor(EBridgeMode e_mode, uint8_t un_duty_cycle = 0);

   class CShaftEncodersInterrupt : public CInterrupt {
   public:
      CShaftEncodersInterrupt(CDifferentialDriveSystem* pc_differential_drive_system,
                              uint8_t un_intr_vect_num);
                              
      void Enable();
      void Disable();
   private:
      void ServiceRoutine();
   private:
      CDifferentialDriveSystem* m_pcDifferentialDriveSystem;
      volatile uint8_t m_unPortLast;
   } m_cShaftEncodersInterrupt;

   class CPIDControlStepInterrupt : public CInterrupt {
   public:
      CPIDControlStepInterrupt(CDifferentialDriveSystem* pc_differential_drive_system, 
                               uint8_t un_intr_vect_num);
      void Enable();
      void Disable();
      void SetTargetVelocity(int16_t velocity, int16_t time);
       int16_t getTargetVelocity();
       int16_t GetErrorListMember(int16_t num);
       void copyErrorList(int16_t errorL[]);
       int16_t GetErrorListLgh();
       int16_t getAveragedError();
       int16_t getLastError();
       int16_t getErrorDeriv();
       int32_t getErrorInt();

       void SetPIDParams(float f_Kp, float f_Ki, float f_Kd);
   private:
      void ServiceRoutine();
   private:   
      CDifferentialDriveSystem* m_pcDifferentialDriveSystem;      

      int16_t m_nTarget;
      int16_t m_nTime;
      int16_t m_nErrListCount;
      int16_t m_nLastError;
      int16_t m_nPostLastError;
      int16_t m_nItrCount;
      int32_t m_nErrorIntegral;

      int16_t m_nErrorList[3];


       bool targetSpeedReverse;

      float m_fOutput;

      float m_fKp;
      float m_fKi;
      float m_fKd;
   } m_cPIDControlStepInterrupt;

   friend CShaftEncodersInterrupt;
   friend CPIDControlStepInterrupt;

   /* Actual step count variable */

    volatile int16_t m_nSteps;
   /* Cached step count variable */

    volatile int16_t m_nStepsOut;
};

#endif
