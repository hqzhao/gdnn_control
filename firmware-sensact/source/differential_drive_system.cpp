
#include "differential_drive_system.h"

#include <firmware.h>

/* Port B Pins - Power and faults */
#define DRV8833_EN     0x08
#define DRV8833_FAULT  0x02

/* Port C Pins - Encoder input */
#define ENC_RIGHT_CHA  0x01
#define ENC_RIGHT_CHB  0x02
#define ENC_LEFT_CHA   0x04
#define ENC_LEFT_CHB   0x08

#define ENC_A 0x01
#define ENC_B 0x02

/* Port D Pins - Motor output */
#define LEFT_CTRL_PIN  0x04
#define RIGHT_CTRL_PIN 0x08
#define RIGHT_MODE_PIN 0x10
#define RIGHT_PWM_PIN  0x20 //port D5
#define LEFT_PWM_PIN   0x40 //port D6
#define LEFT_MODE_PIN  0x80

#define PWM_PIN_A 0x20
#define PWM_PIN_B 0x40

/****************************************/
/****************************************/


CDifferentialDriveSystem::CDifferentialDriveSystem() :
   m_cShaftEncodersInterrupt(this, PCINT1_vect_num),
   m_cPIDControlStepInterrupt(this, TIMER1_COMPA_vect_num),
   m_nSteps(0) {

   /* Initialise pins in a disabled, coasting state */
   PORTB &= ~(DRV8833_EN);
   PORTD &= ~(PWM_PIN_A  |
              PWM_PIN_B);

   /* set the direction of the output pins to output */
   DDRB |= (DRV8833_EN);
   // Deze pins are outputs
   DDRD |= (PWM_PIN_A  |
            PWM_PIN_B);

   //Turn on pull up resistors

    //DDRC =0;
    //PORTC |= (1 << PORTC0)| (1 << PORTC1);

    /* Setup up timer 0 for PWM */
   /* Select phase correct, non-inverting PWM mode on channel A & B */
   TCCR0A |= (1 << WGM00);
   /* Set precaler to 1 */
   TCCR0B |= (1 << CS00);

   /* Initialize left and right motor duty cycle to zero */
   OCR0A = 0;
   OCR0B = 0;

   /* CTC Mode , with precaler set to 64, OCR1A = 2039 (61.275Hz update frequency) */
   TCCR1B |= (1 << WGM12) | (1 << CS11) | (1 << CS10);
   OCR1A = 20000;
   
   /* Enable port change interrupts for right encoder A/B
      and left encoder A/B respectively */
   //PC0 AND PC 1 enabled
   PCMSK1 |= (1 << PCINT8)  | (1 << PCINT9);
   //PCMSK1 |= (1 << PCINT10) | (1 << PCINT11);
   //PCMSK1 |= (1 << PCINT10) ;
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::SetTargetVelocity(int16_t velocity, int16_t time) {
   m_cPIDControlStepInterrupt.SetTargetVelocity(velocity, time);
}

/****************************************/
/****************************************/


int16_t CDifferentialDriveSystem::GetVelocity() {
   int16_t nVelocity;
   uint8_t unSREG = SREG;
   cli();
   nVelocity = m_nStepsOut;
   SREG = unSREG;
   return nVelocity;
}

int16_t CDifferentialDriveSystem::GetErrorListMember(int16_t num) {
   int16_t errorMember;
   uint8_t unSREG = SREG;
   cli();
   errorMember = m_cPIDControlStepInterrupt.GetErrorListMember(num);
   SREG = unSREG;
   return  errorMember;

}

int16_t CDifferentialDriveSystem::GetAveragedError() {
   int16_t errorA;
   errorA = m_cPIDControlStepInterrupt.getAveragedError();
   return  errorA;

}

void CDifferentialDriveSystem::GetCopyErrorList(int16_t errorL[]) {
   m_cPIDControlStepInterrupt.copyErrorList(errorL);
}



int16_t CDifferentialDriveSystem::GetErrorListLgh() {
   int16_t errorLgh;
   uint8_t unSREG = SREG;
   cli();
   errorLgh = m_cPIDControlStepInterrupt.GetErrorListLgh();
   SREG = unSREG;
   return  errorLgh;

}

int16_t CDifferentialDriveSystem::GetTargetVelocity() {
   int16_t tVelocity;
   tVelocity = m_cPIDControlStepInterrupt.getTargetVelocity();
   return tVelocity;
}

int32_t CDifferentialDriveSystem::GetErrorInt() {
   int32_t tErrorInt;
   tErrorInt = m_cPIDControlStepInterrupt.getErrorInt();
   return tErrorInt;
}

/****************************************/
/****************************************/


/****************************************/
/****************************************/

void CDifferentialDriveSystem::Enable() {
   /* Enable the shaft encoder interrupt */
   m_cShaftEncodersInterrupt.Enable();
   /* Enable the PID controller interrupt */
   m_cPIDControlStepInterrupt.Enable();
   /* Enable the motor driver */
   PORTB |= (DRV8833_EN);
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::Disable() {
   /* Disable the motor driver */
   PORTB &= ~(DRV8833_EN);
   /* Disable the PID controller interrupt */
   m_cPIDControlStepInterrupt.Disable();
   /* Disable the shaft encoder interrupt */
   m_cShaftEncodersInterrupt.Disable();
}



void CDifferentialDriveSystem::ConfigureMotor(CDifferentialDriveSystem::EBridgeMode e_mode,
                                                  uint8_t un_duty_cycle) {
   switch(e_mode) {

      case EBridgeMode::FORWARD_PWM_FD:
         PORTD |= (PWM_PIN_A | PWM_PIN_B);
         break;
      case EBridgeMode::REVERSE_PWM_FD:
         PORTD |= (PWM_PIN_A | PWM_PIN_B);
           break;
      case EBridgeMode::COAST:
          PORTD &= ~(PWM_PIN_A | PWM_PIN_B);
           break;
      case EBridgeMode::FORWARD:
         PORTD |= (PWM_PIN_A | PWM_PIN_B);

           break;
      case EBridgeMode::REVERSE:
         PORTD |= (PWM_PIN_A | PWM_PIN_B);
           break;
      case EBridgeMode::BRAKE:
         PORTD |= (PWM_PIN_A | PWM_PIN_B);
           break;
   }

   if(e_mode == EBridgeMode::COAST   ||
      e_mode == EBridgeMode::REVERSE ||
      e_mode == EBridgeMode::FORWARD ||
      e_mode == EBridgeMode::BRAKE) {
      /* disconnect PWM A from the output pin */
      OCR0A = 0;
      TCCR0A &= ~((1 << COM0A1) | (1 << COM0A0));
      /* disconnect PWM B from the output pin */
      OCR0B = 0;
      TCCR0A &= ~((1 << COM0B1) | (1 << COM0B0));
   }
   if(e_mode == EBridgeMode::FORWARD_PWM_FD ||
      e_mode == EBridgeMode::FORWARD_PWM_SD
      ){
      /* connect PWM to A and B = 0*/
      OCR0A = un_duty_cycle;
      TCCR0A |= (1 << COM0A1);
      OCR0B = 0;
      TCCR0A |= (1 << COM0B1);
   }
   if(e_mode == EBridgeMode::REVERSE_PWM_FD ||
      e_mode == EBridgeMode::REVERSE_PWM_SD
      ){
      /* connect PWM to B and A = 0*/
       OCR0A = 0;
       TCCR0A |= (1 << COM0A1);
       OCR0B = un_duty_cycle;
       TCCR0A |= (1 << COM0B1);
   }
}

/****************************************/
/****************************************/

CDifferentialDriveSystem::CShaftEncodersInterrupt::CShaftEncodersInterrupt(
   CDifferentialDriveSystem* pc_differential_drive_system,
   uint8_t un_intr_vect_num) :
   m_pcDifferentialDriveSystem(pc_differential_drive_system),
   m_unPortLast(0) {
   Register(this, un_intr_vect_num);
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::CShaftEncodersInterrupt::Enable() {
   /* clear variables */
   m_unPortLast = 0;
   m_pcDifferentialDriveSystem->m_nSteps = 0;
   /* enable interrupt */
   PCICR |= (1 << PCIE1);
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::CShaftEncodersInterrupt::Disable() {
   /* disable interrupt */
   PCICR &= ~(1 << PCIE1);
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::CShaftEncodersInterrupt::ServiceRoutine() {

   uint8_t unPortSnapshot = PINC;
   //UDR0=PINC;
   uint8_t unPortDelta = m_unPortLast ^ unPortSnapshot;
   /* This intermediate value determines whether the motors are moving
      backwards or forwards. The expression uses Reed-Muller logic and
      leverages the symmetry of the encoder inputs */
   uint8_t unIntermediate = (~unPortSnapshot) ^ (m_unPortLast >> 1);
   //m_pcDifferentialDriveSystem->m_nSteps++;
   /* check the encoder */
   //m_pcDifferentialDriveSystem->m_nSteps++;

   //if(m_pcDifferentialDriveSystem->m_nSteps>15) m_pcDifferentialDriveSystem->m_nSteps=0;


   if(unPortDelta & (ENC_A | ENC_B)) {
   if(unIntermediate & ENC_A) {
      m_pcDifferentialDriveSystem->m_nSteps--;
   }
   else{
      m_pcDifferentialDriveSystem->m_nSteps++;
   }


   }







   m_unPortLast = unPortSnapshot;
}

/****************************************/
/****************************************/

CDifferentialDriveSystem::CPIDControlStepInterrupt::CPIDControlStepInterrupt(
   CDifferentialDriveSystem* pc_differential_drive_system,
   uint8_t un_intr_vect_num) :
   m_pcDifferentialDriveSystem(pc_differential_drive_system),
   m_nTarget(0),
   m_nTime(0),
   m_nErrListCount(0),
   m_nLastError(0),
   m_nPostLastError(0),
   m_nErrorIntegral(0.0f),
   m_nItrCount(0),

   m_fOutput(0.0f),
   /* office */
   //m_fKp(0.75f),
   //m_fKi(0.00f),
   //m_fKd(0.35f) {
   /* arena */
   m_fKp(0.00f),
   m_fKi(0.00f),
   m_fKd(0.00f) {
   Register(this, un_intr_vect_num);
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::CPIDControlStepInterrupt::Enable() {
   /* clear intermediate variables */
   m_nLastError = 0;
   m_nPostLastError = 0;
   m_nErrorIntegral = 0;
   m_nTarget = 0;
   m_nTime = 0;
   m_nErrListCount =0;
   m_nItrCount = 0;
   /* enable interrupt */
   TIMSK1 |= (1 << OCIE1A);
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::CPIDControlStepInterrupt::Disable() {
   /* disable interrupt */
   TIMSK1 &= ~(1 << OCIE1A);
}

/****************************************/
/****************************************/

int16_t CDifferentialDriveSystem::CPIDControlStepInterrupt::getTargetVelocity(){
   int16_t tVelocity;
   uint8_t unSREG = SREG;
   cli();
   tVelocity = m_nTarget;
   SREG = unSREG;
   return tVelocity;
}

/****************************************/
/****************************************/

int16_t CDifferentialDriveSystem::CPIDControlStepInterrupt::GetErrorListMember(int16_t num){
   int16_t errorM;
   uint8_t unSREG = SREG;
   cli();
   errorM = m_nErrorList[num];
   SREG = unSREG;
   return errorM;
}

int16_t CDifferentialDriveSystem::CPIDControlStepInterrupt::getAveragedError(){
   int16_t errorA=0;
   //int16_t errorL;
   uint8_t unSREG = SREG;
   cli();
   //errorL = m_nErrListCount;
   for(int16_t idx=0;idx<3;idx++)errorA+=((m_nErrorList[idx])/3);
   m_nErrListCount=0;
   SREG = unSREG;

   //errorA*=100;
   //errorA/=errorL;
   return errorA;
}

void CDifferentialDriveSystem::CPIDControlStepInterrupt::copyErrorList(int16_t errorL[]) {
   uint8_t unSREG = SREG;
   cli();
   for(int16_t idx=0;idx<3;idx++)errorL[idx]=m_nErrorList[idx];
   SREG = unSREG;
}
/****************************************/
/****************************************/

int16_t CDifferentialDriveSystem::CPIDControlStepInterrupt::GetErrorListLgh(){
   int16_t errorL;
   uint8_t unSREG = SREG;
   cli();
   errorL = m_nErrListCount;
   SREG = unSREG;
   return errorL;
}

/****************************************/
/****************************************/
int16_t CDifferentialDriveSystem::CPIDControlStepInterrupt::getLastError() {
   int16_t errorLast;
   uint8_t unSREG = SREG;
   cli();
   errorLast = m_nLastError;
   SREG = unSREG;
   return errorLast;
}

/****************************************/
/****************************************/
int32_t CDifferentialDriveSystem::CPIDControlStepInterrupt::getErrorInt() {
   int32_t errorInt;
   uint8_t unSREG = SREG;
   cli();
   errorInt = m_nErrorIntegral;
   SREG = unSREG;
   return errorInt;
}
/****************************************/
/****************************************/


void CDifferentialDriveSystem::CPIDControlStepInterrupt::SetTargetVelocity(int16_t velocity, int16_t time) {
   uint8_t unSREG = SREG;
   cli();
   //if(velocity*m_nTarget<0){
      //m_fOutput=m_fOutput*((float)velocity/(float)m_nTarget);
   //   m_fOutput=-m_fOutput;
   //}
   //m_fOutput=0;
   m_nTarget = velocity;
   m_nTime = time;
   m_nErrListCount = 0;
   //empty error
   m_nErrorList[2]=0;
   m_nErrorList[1]=0;
   m_nErrorList[0]=0;
   m_nItrCount=0;
   m_nLastError=0;
   m_nPostLastError=0;
   SREG = unSREG;
}


/****************************************/
/****************************************/

void CDifferentialDriveSystem::CPIDControlStepInterrupt::ServiceRoutine() {
   /* Calculate left PID intermediates */
   //nTime, execution time count

   if(m_nTime>0){
      /*

     if(m_nItrCount!=9){
        m_nItrCount+=1;
        m_nTime-=1;
     }
     else{
     */
         int16_t nError = m_nTarget - (m_pcDifferentialDriveSystem->m_nSteps);
         //record response

         //record last three errors
         m_nErrorList[2]=m_nErrorList[1];
         m_nErrorList[1]=m_nErrorList[0];
         m_nErrorList[0]=nError;
         /*
         if(m_nErrListCount<3){
            m_nErrorList[m_nErrListCount] = nError;
            m_nErrListCount +=1;
         }
         else{
            m_nErrListCount=0;
            m_nErrorList[m_nErrListCount] = nError;
            m_nErrListCount +=1;
         }
          */



         /* Accumulate the integral component */
         m_nErrorIntegral = nError - 2*m_nLastError + m_nPostLastError;
         /* Calculate the derivate component */
         int16_t nErrorDerivative = (nError - m_nLastError);
         m_nPostLastError = m_nLastError;
         m_nLastError = nError;
         /* Calculate output value */

         m_fOutput +=
                 (m_fKi * nError) +
                 (m_fKd * m_nErrorIntegral) +
                 (m_fKp * nErrorDerivative);
         /* Limit output */
         /* TODO: Note that we are saturating the PID output value which is reused */
         uint8_t maxControle=180;

         m_fOutput = (m_fOutput < float(maxControle)) ? m_fOutput : float(maxControle);
         m_fOutput = (m_fOutput >-float(maxControle)) ? m_fOutput :-float(maxControle);
         /* store the sign of the output */
         bool bNegative = (m_fOutput < 0.0f);


         //bNegative=false;
         //bNegative=true;

         /* take the absolute value */
         float fOutput = (bNegative ? -m_fOutput : m_fOutput);

         /* saturate into the uint8_t range */
         uint8_t unDutyCycle=uint8_t(fOutput);


         /* Update right motor */

         m_pcDifferentialDriveSystem->ConfigureMotor(
                 bNegative ? CDifferentialDriveSystem::EBridgeMode::REVERSE_PWM_FD:
                 CDifferentialDriveSystem::EBridgeMode::FORWARD_PWM_FD,
                 unDutyCycle);


         /* copy the step counters for velocity measurements */
         m_pcDifferentialDriveSystem->m_nStepsOut = (m_pcDifferentialDriveSystem->m_nSteps);
         /* clear the step counters */
         m_pcDifferentialDriveSystem->m_nSteps = 0;
         m_nItrCount=0;
         m_nTime-=1;
      //}
   }
   else{
      uint8_t unDutyCycle=uint8_t(0);
      m_pcDifferentialDriveSystem->ConfigureMotor(CDifferentialDriveSystem::EBridgeMode::FORWARD_PWM_FD, unDutyCycle);
      //empty error list
      m_nErrorList[2]=0;
      m_nErrorList[1]=0;
      m_nErrorList[0]=0;
      m_pcDifferentialDriveSystem->m_nSteps = 0;
      m_nItrCount=0;
      m_nLastError=0;
      m_nPostLastError=0;
   }



}

/****************************************/
/****************************************/
void CDifferentialDriveSystem::SetPIDParams(float f_Kp, float f_Ki, float f_Kd) {
   m_cPIDControlStepInterrupt.SetPIDParams(f_Kp, f_Ki, f_Kd);
}

/****************************************/
/****************************************/

void CDifferentialDriveSystem::CPIDControlStepInterrupt::SetPIDParams(float f_Kp, float f_Ki, float f_Kd) {
   uint8_t unSREG = SREG;
   cli();
   m_fKp = f_Kp;
   m_fKi = f_Ki;
   m_fKd = f_Kd;
   SREG = unSREG;
}

/****************************************/
/****************************************/







