GDNN control software on Arduino side,

Only minor change in original ATmega 328 control software

* Requirements:

	Arduino nano, Arduino development kit

* Upload code to four Arduino boards:

1. Make in root folder;
	
2. For each Arduino board, in this folder, execute: 

[path to avrdude in Arduino kits] -C[path to avrdude condigure file] -v -patmega328p -carduino -P[port of micro-controller] -b115200 -D -Uflash:w:build/firmware.hex

For example:

/Users/hzhao/Library/Arduino15/packages/arduino/tools/avrdude/6.3.0-arduino14/bin/avrdude -C/Users/hzhao/Library/Arduino15/packages/arduino/tools/avrdude/6.3.0-arduino14/etc/avrdude.conf -v -patmega328p -carduino -P/dev/tty.usbserial-AC0092BA -b115200 -D -Uflash:w:build/firmware.hex

