//
// Created by hanqing_zhao on 2019/5/9.
//

#include "GDNN3L.h"
#define N  999

void reseauGDNN3L::initiateGDNN(std::string envFileName) {
    int numNodes = 43;
    //init bias to 0
    bias = new double[numNodes];
    nodesOutLast = new double[numNodes];

    for(int idx=0; idx<numNodes;idx++) bias[idx]=0;
    for(int idx=0; idx<numNodes;idx++) nodesOutLast[idx]=0;

    //init weights
    weight = new double*[numNodes];
    for(int idx=0;idx<numNodes;idx++){
        weight[idx]=new double[numNodes];
    }

    //try to read parameters from configure file
    int succ=readParameterFromFile(envFileName);

    //init from file failed, randomly init
    if(succ==0){
        // nodes 0 to 3, three input nodes, 4 to 18, hidden nodes, 19 to 30 output nodes
        for(int idx=0; idx<4;idx++){
            //forward connection from input to first layer
            for(int idy=4; idy<19;idy++) {
                weight[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }
        //delayed propagation backwards
        for(int idx=4; idx<19;idx++){
            //forward connection from input to first layer
            for(int idy=0; idy<4;idy++) {
                weight[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }
        //hidden propagation forwards
        for(int idx=4; idx<19;idx++){
            //forward connection from h1 to h2
            for(int idy=19; idy<31;idy++) {
                weight[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }
        //hidden propagation forwards towards output
        for(int idx=4; idx<19;idx++){
            //forward connection from h2 to out
            for(int idy=31; idy<43;idy++) {
                weight[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }
    }
}

int reseauGDNN3L::forwardProb(double *inputLayer, double *outputLayer,bool changeOutLast) {
    //intermediary output

    double nodeOut[43];

    for(int idx=0; idx<43; idx++) nodeOut[idx]=0;
    //output of four input nodes
    for(int idx=0;idx<4;idx++){
        ///comment this for loop for a GDNN without recurrent connection
        for(int idy=4;idy<19;idy++){
            nodeOut[idx]+=weight[idy][idx]*nodesOutLast[idy];
        }

        nodeOut[idx]+=inputLayer[idx];
        nodeOut[idx]=tanh(nodeOut[idx]+bias[idx]);
    }

    //output for 15 hidden 1 layer nodes
    for(int idx=4; idx<19;idx++){
        for(int idy=0;idy<4;idy++){
            nodeOut[idx]+=weight[idy][idx]*nodeOut[idy];
        }
        nodeOut[idx]=tanh(nodeOut[idx]+bias[idx]);
    }

    //output for 12 hidden 2 layer nodes
    for(int idx=19;idx<31;idx++){
        for(int idy=4;idy<19;idy++){
            nodeOut[idx]+=weight[idy][idx]*nodeOut[idy];
        }
        nodeOut[idx]=sigmoid(nodeOut[idx]+bias[idx]);
    }

    //output for 12 output nodes
    for(int idx=31;idx<43;idx++){
        for(int idy=19;idy<31;idy++){
            nodeOut[idx]+=weight[idy][idx]*nodeOut[idy];
        }
        nodeOut[idx]=sigmoid(nodeOut[idx]+bias[idx]);
        outputLayer[idx-31]=nodeOut[idx];
    }

    //change last output record if
    if(changeOutLast){
        for(int idx=0;idx<43;idx++){
            nodesOutLast[idx]=nodeOut[idx];
        }
    }
    return 0;
}



double reseauGDNN3L::sigmoid(double x) {
    double exp_value;
    double return_value;

    /*** Exponential calculation ***/
    exp_value = exp((double) -x);

    /*** Final sigmoid value ***/
    return_value = 1 / (1 + exp_value);

    return return_value;
}

int reseauGDNN3L::backwardprob(double *inputLayer, double **vError, double learningRate) {
    double newWeight[43][43];
    double dervEop[12], origOut[12], deltaOut[12], dervWoe[12];

    //derv1 derivative of network output and weight
    //derv2 derivative of error func and network output
    double derv1, derv2;

    //calculate derivation of error func to each output
    for(int idx=0;idx<12;idx++){
        dervEop[idx]=-(vError[idx/3][0]);
        //if for KP
        if(idx%3==0) dervEop[idx]*=(vError[idx/3][0]-vError[idx/3][1]);
        //if for kI
        if(idx%3==1) dervEop[idx]*=(vError[idx/3][0]);
        //if for kD
        if(idx%3==2) dervEop[idx]*=(vError[idx/3][0]-2*vError[idx/3][1]+vError[idx/3][2]);
    }

    //calculate original output
    forwardProb(inputLayer, origOut, false);

    //calculate derivation of each output to each weight and update weight
    for(int idx=0;idx<43;idx++){
        for(int idy=0;idy<43;idy++){
            newWeight[idy][idx]=weight[idy][idx];

            weight[idy][idx]+=1e-7;

            forwardProb(inputLayer, deltaOut, false);
            weight[idy][idx]-=1e-7;

            for(int idz=0; idz<12; idz++){
                dervWoe[idz]=(deltaOut[idz]-origOut[idz])/(1e-7);
                newWeight[idy][idx]-=learningRate*dervWoe[idz]*dervEop[idz];
            }

        }
    }


    // renew original weights
    for(int idx=0;idx<43;idx++){
        for(int idy=0;idy<43;idy++){
            weight[idy][idx]=newWeight[idy][idx];
        }
    }



}

int reseauGDNN3L::readParameterFromFile(std::string envFileName) {
    std::ifstream infile(envFileName);
    //if no file found
    if(!infile){
        std::cout<<"GDNN parameter file not found, randomly init "<<std::endl;
        return 0;
    }
    std::string line;
    std::getline(infile, line);
    for(int idx=0; idx<43; idx++){
        for(int idy=0; idy<43; idy++){
            weight[idx][idy]=std::stof(line);
            std::getline(infile, line);
        }

    }
    infile.close();
    std::cout<<"GDNN parameter read successfully "<<std::endl;
    return 1;

}

int reseauGDNN3L::saveParameterToFile(std::string envFileName) {
    std::ofstream outfile;
    outfile.open(envFileName);
    outfile<< std::setprecision(8) << std::fixed;
    for(int idx=0; idx<43; idx++){
        for(int idy=0; idy<43; idy++){
            outfile << weight[idx][idy] << std::endl;
        }

    }

    outfile.close();
}