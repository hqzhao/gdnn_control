//
// Created by hanqing_zhao on 2019/3/29.
//

#ifndef DEBUGGL_GDNNS_H
#define DEBUGGL_GDNNS_H
#include <cstdlib>
#include <math.h>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>


class reseauGDNNS{
    double **weight1;
    double **weight2;
    double **weight3;
    double **weight4;
    double *bias1;
    double *bias2;
    double *bias3;
    double *bias4;
    double *nodesOutLast1;
    double *nodesOutLast2;
    double *nodesOutLast3;
    double *nodesOutLast4;
public:
    void initiateGDNN(std::string envFileName);
    int forwardProb(double* inputLayer, double* outputLayer,bool changeOutLast=true);
    double sigmoid(double x);
    int backwardprob(double* inputLayer, double** vError, double learningRate);
    int saveParameterToFile(std::string envFileName);
    int readParameterFromFile(std::string envFileName);


};


#endif //DEBUGGL_GDNNS_H
