//
// Created by hanqing_zhao on 2019/6/5.
//

#ifndef GDNN_CTRL_UP_UTILS_H
#define GDNN_CTRL_UP_UTILS_H
/*------------ Function Control Channels ------------*/
extern double CH1,CH1_MAX,CH1_MIN,CH1_STEP;
extern double CH2,CH2_MAX,CH2_MIN,CH2_STEP;
extern int CMDCH1;
extern int CMDCH2;

#define MAXLOG 1000
#define MAXDATATRACK 6
extern int dataCount;
extern double dataLog[MAXDATATRACK][MAXLOG];

int function_init();
int function_exit();
int combinedDrive(double allerA, double allerV, double rotationV, double *velocity, int dTime);
int getVelocity(double allerA, double allerV, double rotationV, double *velocity);
int combineSendPidParam(double *pidParam);
int uartGetError(int MotorNum);
int uartGetError(int MotorNum, double errorL[]);
int uartGetInt(int MotorNum);


#endif //GDNN_CTRL_UP_UTILS_H
