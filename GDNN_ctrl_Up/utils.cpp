/*---------------------------------------------------------*/
/*
   	Hanqing ZHAO,

   	hanqing.zhao@ulb.ac.be

*/
/*---------------------------------------------------------*/

#include <stdio.h>
#include "utils.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>


#define PI 3.1415926

#include "packet_control_interface.h"

/*------------ Function Control Channels ------------*/
double CH1 = 0, CH1_MAX = 60, CH1_MIN = -60, CH1_STEP = 0.3;
double CH2 = 0, CH2_MAX = 60, CH2_MIN = -60, CH2_STEP = 0.3;
int CMDCH1 = 0;
int CMDCH2 = 0;

/* --------------- uart declare --------------------*/
int uartOpen();
int uartSendSpeed(int16_t M1, int16_t M2, int16_t M3, int16_t M4, int16_t dTime);
int uartGetSpeed(int16_t *S, int MotorNum);
int uartSendPIDParams(float kp, float ki, float kd, int MotorNum);
int uartGetSpeedWithDebug(int16_t *L, int16_t *R, int16_t *LError, float *LOutput);

/* --------------- variables --------------------*/
int16_t tL, tR;
int16_t mL, mR;
float Kp, Ki, Kd;

/* --------------- MainLoop functions --------------------*/
int function_exit()
{
	uartSendSpeed(0,0,0,0,100);
	return 0;
}

int function_init()
{
	tL = 0;
	tR = 0;
	Kp = 0;
	Ki = 0;
	Kd = 0;

	uartOpen();

	return 0;
}
//send a sriving command to four microcontrollers
int combinedDrive(double allerA, double allerV, double rotationV, double *velocity, int driveTime)
{
    double vx,vy;
    int v1,v2,v3,v4;
    vx=allerV*cos((allerA/180)*PI);
    vy=allerV*sin((allerA/180)*PI);

    v1=vy+rotationV;
    v2=-vx+rotationV;
    v3=-vy+rotationV;
    v4=vx+rotationV;
    std::cout<<"v1-v4 "<<v1<<" "<<v2<<" "<<v3<<" "<<v4<<std::endl;
    uartSendSpeed(v1,v2,v3,v4,driveTime);
    velocity[0]=(double)v1/200;
	velocity[1]=(double)v2/200;
	velocity[2]=(double)v3/200;
	velocity[3]=(double)v4/200;
	//uartSendSpeed(10,10,10,10);
    return 0;
}
//decompose driving velocity to rotation speed of four wheels
int getVelocity(double allerA, double allerV, double rotationV, double *velocity){
    double vx,vy;
    int v1,v2,v3,v4;
    vx=allerV*cos((allerA/180)*PI);
    vy=allerV*sin((allerA/180)*PI);

    v1=vy+rotationV;
    v2=-vx+rotationV;
    v3=-vy+rotationV;
    v4=vx+rotationV;


    velocity[0]=(double)v1/200;
    velocity[1]=(double)v2/200;
    velocity[2]=(double)v3/200;
    velocity[3]=(double)v4/200;

    return 0;
}
//send PID parameters to four motors
int combineSendPidParam(double *pidParam){
	for(int idm=0;idm<4;idm++){
		uartSendPIDParams((float)(pidParam[idm*3]), (float)(pidParam[idm*3+1]), (float)(pidParam[idm*3+2]), idm+1);
		std::cout<<"motor "<<idm+1<<"pid params "<<pidParam[idm*3]<<" "<<pidParam[idm*3+1]<<" "<<pidParam[idm*3+2]<<std::endl;
	}

}


int motorStop()
{
    uartSendSpeed(0,0,0,0,100);
    return 0;
}


/*---- uart --------------------------------------------------*/
CPacketControlInterface *ddsInterfaceM1,*ddsInterfaceM2,*ddsInterfaceM3,*ddsInterfaceM4;
int uartOpen()
{
	ddsInterfaceM1 =
			new CPacketControlInterface("pm", "/dev/ttyUSB0", 9600);
    ddsInterfaceM2 =
			new CPacketControlInterface("pm", "/dev/ttyUSB1", 9600);
    ddsInterfaceM3 =
			new CPacketControlInterface("pm", "/dev/ttyUSB2", 9600);
    ddsInterfaceM4 =
			new CPacketControlInterface("pm", "/dev/ttyUSB3", 9600);
	if (!ddsInterfaceM1->Open())
		{   printf("Motor 1 port open failed\n");  }
    if (!ddsInterfaceM2->Open())
    {   printf("Motor 2 port open failed\n");  }
    if (!ddsInterfaceM3->Open())
    {   printf("Motor 3 port open failed\n");}
    if (!ddsInterfaceM4->Open())
    {   printf("Motor 4 port open failed\n"); }

	printf("---Establish Connection with MCUs---------------------\n");

	printf("---Initialize Actuator---------------------\n");
	enum class EActuatorInputLimit : uint8_t {
		LAUTO = 0, L100 = 1, L150 = 2, L500 = 3, L900 = 4
	};

	/* Override actuator input limit to 100mA */
	/*
	pmInterface->SendPacket(
			CPacketControlInterface::CPacket::EType::SET_ACTUATOR_INPUT_LIMIT_OVERRIDE,
			static_cast<const uint8_t>(EActuatorInputLimit::L900));
*/
	/* Enable the actuator power domain */
	/*
	pmInterface->SendPacket(
			CPacketControlInterface::CPacket::EType::SET_ACTUATOR_POWER_ENABLE, 
			true);
			*/
				      
	/* Power up the differential drive system */
	ddsInterfaceM1->SendPacket(CPacketControlInterface::CPacket::EType::SET_DDS_ENABLE, true);
    ddsInterfaceM2->SendPacket(CPacketControlInterface::CPacket::EType::SET_DDS_ENABLE, true);
    ddsInterfaceM3->SendPacket(CPacketControlInterface::CPacket::EType::SET_DDS_ENABLE, true);
    ddsInterfaceM4->SendPacket(CPacketControlInterface::CPacket::EType::SET_DDS_ENABLE, true);
	/* Initialize the differential drive system */
	uint8_t pnStopDriveSystemData[] = {0, 0, 0, 0};
	ddsInterfaceM1->SendPacket(
			CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
			pnStopDriveSystemData,
			sizeof(pnStopDriveSystemData));
    ddsInterfaceM2->SendPacket(
            CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
            pnStopDriveSystemData,
            sizeof(pnStopDriveSystemData));
    ddsInterfaceM3->SendPacket(
            CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
            pnStopDriveSystemData,
            sizeof(pnStopDriveSystemData));
    ddsInterfaceM4->SendPacket(
            CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
            pnStopDriveSystemData,
            sizeof(pnStopDriveSystemData));

	printf("initialize complete\n");

	return 0;
}
int uartSendSpeed(int16_t M1, int16_t M2, int16_t M3, int16_t M4, int16_t dTime)
{
	uint8_t uartData[] = {
		reinterpret_cast<uint8_t*>(&M1)[1],
		reinterpret_cast<uint8_t*>(&M1)[0],
		reinterpret_cast<uint8_t*>(&dTime)[1],
		reinterpret_cast<uint8_t*>(&dTime)[0],
	};
	ddsInterfaceM1->SendPacket(
		CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
		uartData,
		sizeof(uartData));


	//set speed motor M2
    uartData[0] = reinterpret_cast<uint8_t*>(&M2)[1];
    uartData[1] = reinterpret_cast<uint8_t*>(&M2)[0];
    uartData[2] = reinterpret_cast<uint8_t*>(&dTime)[1];
    uartData[3] = reinterpret_cast<uint8_t*>(&dTime)[0];

    ddsInterfaceM2->SendPacket(
            CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
            uartData,
            sizeof(uartData));

    //set speed motor M3
    uartData[0] = reinterpret_cast<uint8_t*>(&M3)[1];
    uartData[1] = reinterpret_cast<uint8_t*>(&M3)[0];
    uartData[2] = reinterpret_cast<uint8_t*>(&dTime)[1];
    uartData[3] = reinterpret_cast<uint8_t*>(&dTime)[0];

    ddsInterfaceM3->SendPacket(
            CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
            uartData,
            sizeof(uartData));

    //set speed motor M4
    uartData[0] = reinterpret_cast<uint8_t*>(&M4)[1];
    uartData[1] = reinterpret_cast<uint8_t*>(&M4)[0];
    uartData[2] = reinterpret_cast<uint8_t*>(&dTime)[1];
    uartData[3] = reinterpret_cast<uint8_t*>(&dTime)[0];

    ddsInterfaceM4->SendPacket(
            CPacketControlInterface::CPacket::EType::SET_DDS_SPEED,
            uartData,
            sizeof(uartData));

	return 0;
}
int uartSendPIDParams(float kp, float ki, float kd, int MotorNum)
{
	uint8_t uartData[] = {
		reinterpret_cast<uint8_t*>(&kp)[3],
		reinterpret_cast<uint8_t*>(&kp)[2],
		reinterpret_cast<uint8_t*>(&kp)[1],
		reinterpret_cast<uint8_t*>(&kp)[0],

		reinterpret_cast<uint8_t*>(&ki)[3],
		reinterpret_cast<uint8_t*>(&ki)[2],
		reinterpret_cast<uint8_t*>(&ki)[1],
		reinterpret_cast<uint8_t*>(&ki)[0],

		reinterpret_cast<uint8_t*>(&kd)[3],
		reinterpret_cast<uint8_t*>(&kd)[2],
		reinterpret_cast<uint8_t*>(&kd)[1],
		reinterpret_cast<uint8_t*>(&kd)[0],
	};
	if(MotorNum==1){
        ddsInterfaceM1->SendPacket(
                CPacketControlInterface::CPacket::EType::SET_DDS_PARAMS,
                uartData,
                sizeof(uartData));
	}
    if(MotorNum==2){
        ddsInterfaceM2->SendPacket(
                CPacketControlInterface::CPacket::EType::SET_DDS_PARAMS,
                uartData,
                sizeof(uartData));
    }

    if(MotorNum==3){
        ddsInterfaceM3->SendPacket(
                CPacketControlInterface::CPacket::EType::SET_DDS_PARAMS,
                uartData,
                sizeof(uartData));
    }
    if(MotorNum==4){
        ddsInterfaceM4->SendPacket(
                CPacketControlInterface::CPacket::EType::SET_DDS_PARAMS,
                uartData,
                sizeof(uartData));
    }

	return 0;
}
int uartGetSpeed(int16_t *S, int MotorNum)
{
	int16_t getL, getR;
    CPacketControlInterface *ddsInterface;
	if(MotorNum==1) ddsInterface=ddsInterfaceM1;
    if(MotorNum==2) ddsInterface=ddsInterfaceM2;
    if(MotorNum==3) ddsInterface=ddsInterfaceM3;
    if(MotorNum==4) ddsInterface=ddsInterfaceM4;

	ddsInterface->SendPacket(CPacketControlInterface::CPacket::EType::GET_DDS_SPEED);

	if(ddsInterface->WaitForPacket(1000, 3))
	{
		if(ddsInterface->GetState() == CPacketControlInterface::EState::RECV_COMMAND)
		{
			const CPacketControlInterface::CPacket& cPacket = ddsInterface->GetPacket();
			if (cPacket.GetType() == CPacketControlInterface::CPacket::EType::GET_DDS_SPEED)
			{
				if(cPacket.GetDataLength() == 4) 
				{
					const uint8_t* punPacketData = cPacket.GetDataPointer();
					reinterpret_cast<int16_t&>(getL) = punPacketData[0]<<8 | punPacketData[1];
					*S = getL;
					return 0;
				} 
			}
		}
	}

	return -1;
}

int uartGetError(int MotorNum, double errorL[]){
	int16_t errC, errP1, errP2;
	CPacketControlInterface *ddsInterface;
	if(MotorNum==1) ddsInterface=ddsInterfaceM1;
	if(MotorNum==2) ddsInterface=ddsInterfaceM2;
	if(MotorNum==3) ddsInterface=ddsInterfaceM3;
	if(MotorNum==4) ddsInterface=ddsInterfaceM4;

	ddsInterface->SendPacket(CPacketControlInterface::CPacket::EType::GET_ERROR_LIST);
	if(ddsInterface->WaitForPacket(100, 3))
	{
		if(ddsInterface->GetState() == CPacketControlInterface::EState::RECV_COMMAND)
		{
			const CPacketControlInterface::CPacket& cPacket = ddsInterface->GetPacket();
			if (cPacket.GetType() == CPacketControlInterface::CPacket::EType::GET_ERROR_LIST)
			{
				if(cPacket.GetDataLength() != 100)
				{
					const uint8_t* punPacketData = cPacket.GetDataPointer();
					reinterpret_cast<int16_t&>(errC) = punPacketData[0]<<8 | punPacketData[1];
                    reinterpret_cast<int16_t&>(errP1) = punPacketData[2]<<8 | punPacketData[3];
                    reinterpret_cast<int16_t&>(errP2) = punPacketData[4]<<8 | punPacketData[5];
                    errorL[0]=(double)errC/70;
                    errorL[1]=(double)errP1/70;
                    errorL[2]=(double)errP2/70;
					return (int)errC;
				}
			}
		}
	}
}

int uartGetInt( int MotorNum){
    int32_t getI;
    CPacketControlInterface *ddsInterface;
    if(MotorNum==1) ddsInterface=ddsInterfaceM1;
    if(MotorNum==2) ddsInterface=ddsInterfaceM2;
    if(MotorNum==3) ddsInterface=ddsInterfaceM3;
    if(MotorNum==4) ddsInterface=ddsInterfaceM4;

    ddsInterface->SendPacket(CPacketControlInterface::CPacket::EType::GET_PID_DERIVATE);
    if(ddsInterface->WaitForPacket(100000, 3))
    {
        if(ddsInterface->GetState() == CPacketControlInterface::EState::RECV_COMMAND)
        {
            const CPacketControlInterface::CPacket& cPacket = ddsInterface->GetPacket();
            if (cPacket.GetType() == CPacketControlInterface::CPacket::EType::GET_PID_DERIVATE)
            {
                if(cPacket.GetDataLength() != 100)
                {
                    const uint8_t* punPacketData = cPacket.GetDataPointer();
                    reinterpret_cast<int32_t&>(getI) = punPacketData[0]<<24 | punPacketData[1]<<16 | punPacketData[2]<<8 | punPacketData[3];
                    return (int)getI;
                }
            }
        }
    }
}

