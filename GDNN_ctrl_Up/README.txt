GDNN control software for omnidirectional driving platform,

* Requirements:

	Intel Up Board, Ubuntu 16.04, C++ 11

* Files and Functions:

	Main.cpp : main function.

	utils.cpp : high level communication functions (send parameters and receive errors).

	packet_control_interface :  basic communication functions, package definition.

	GDNN : implementation of 3-layer Ensemble GDNN network;

	GDNN3L : implementation of 4-layer Ensemble GDNN network;

	GDNNS : implementation of 4-layer Separated GDNN networks;


* Usage:

**Before compiling: 
	
	1. Change USB port names of four micro controllers in line 124-131 inside utils.cpp

	2. If need GDNN with no-recurrent connection, comment the for loop at line 68-70 in GDNN3L.cpp
	
**Compile: make in the root folder:

**Execution: ./GDNN_ctrl_Up


Command for straight driving:

	d [driving direction] [driving speed] [rotation speed] [time]

	Example (drives robot 45 degree speed=50 time = 10):

	d 45 50 0 10 

Command for trajectory execution:

	p [trajectory file name]

	Example (execute star trajectory):

	p star30na15.txt

Command for save and exit: e

Example of an execution of star trajectory, using 4-layer GDNN network:

MacBook-Pro-de-hanqing-zhao:GDNN_ctrl_Up hzhao$ ./GDNN_ctrl_Up
---Establish Connection with MCUs---------------------
---Initialize Actuator---------------------
initialize complete
Enable GDNN ? (y/n)
>y
Please enter environment GDNN file name:
>test1
GDNN type? (separated(s)/ensemble(e))
>e
DRNN hidden layer? (single(s)/multi(m))
>m
Driving scheme? (precise(p)/continue(c)
>p
GDNN parameter file not found, randomly init 
Please enter a valid command:
>p star30na156.txt
Received command: p star30na156.txt

Exec CMD: 45 30 0 15
motor 1pid params 0.5 0.5 0.5
motor 2pid params 0.5 0.5 0.5
motor 3pid params 0.5 0.5 0.5
motor 4pid params 0.5 0.5 0.5
v1-v4 21 -21 -21 21
now time 32017
motor 1 error current 0
...



