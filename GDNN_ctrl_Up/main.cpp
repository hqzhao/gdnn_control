/*---------------------------------------------------------*/
/*
   	Hanqing ZHAO,

   	hanqing.zhao@ulb.ac.be

*/
/*---------------------------------------------------------*/

//#define windows 
//#define ubuntu
#define mac

/*----------------- OpenGL Include -------------------------*/
	// get from CMake


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<iostream>
#include<fstream>
#include<string>
#include <sstream>


#ifdef windows
#include<windows.h>
#endif

//gettime
#include <sys/time.h>
#include <unistd.h>

/*----------------- LoopFunction Include -------------------------*/
#include"GDNN.h"
#include"GDNNS.h"
#include"GDNN3L.h"
#include"GDNN3L.h"
#include"utils.h"
#define pi 3.1415926



/*-----------------------------------------------------------------*/
/*  main start  */
/*-----------------------------------------------------------------*/

using namespace std;

int main(int argc, char* argv[])
{
	////////////////////////////  function init ////////////////////
	if (function_init() != 0)
		return -1;
	//open command list file

	//time storage

    struct timeval start, end;

    long mtime, lastExecT, seconds, useconds;
    //prog start time
    gettimeofday(&start, NULL);
    lastExecT=0;



    //strings for buffer user input
    string parsed, consigne, envName, enableDRNN, errorName, DRNNType,DRNNLayer, drvType;
    errorName = "errorLog.txt";
	double allerA, allerV, rotationV, execTime;
    //number of GDNN training rounds and number of iterruption on arduinos
	double execTFactor=1, iterFactor=1;

    //if rnn training is enabled
	bool enaRNN=false;
    cout << "Enable GDNN ? (y/n)\n>";
    getline(cin, enableDRNN);
    if(enableDRNN[0]=='y'){
        cout << "Please enter environment GDNN file name:\n>";
        getline(cin, envName);
        enaRNN=true;
        cout << "GDNN type? (separated(s)/ensemble(e))\n>";
        getline(cin, DRNNType);
        //if is ensemble
        if(DRNNType[0]!='s'){
            cout << "DRNN hidden layer? (single(s)/multi(m))\n>";
            getline(cin, DRNNLayer);
        }
    }


    cout << "Driving scheme? (precise(p)/continue(c)\n>";
    getline(cin, drvType);

    if(drvType[0]=='p'){
        execTFactor=5;
        iterFactor=2;
    }else{
        execTFactor=10;
        iterFactor=1;
    }

	std::ofstream errorFile;
	errorFile.open(errorName);
    reseauGDNN* GDNN;
    reseauGDNNS* GDNNS;
    reseauGDNN3L* GDNN3L;

    if(DRNNType[0]=='s'){
        GDNNS = new reseauGDNNS();
        GDNNS->initiateGDNN(envName);
    }
    else{
        if(DRNNLayer[0]=='s'){
            GDNN = new reseauGDNN();
            GDNN->initiateGDNN(envName);
        }
        else{
            GDNN3L = new reseauGDNN3L();
            GDNN3L->initiateGDNN(envName);
        }

    }



    //init data for PID control learning


    double inputLayer[4],outputLayer[12];
    for(int idx=0;idx<4;idx++) inputLayer[idx]=1;
    if(DRNNType[0]=='s'){
        GDNNS->forwardProb(inputLayer,outputLayer,false);
    }
    else{
        if(DRNNLayer[0]=='s') {
            GDNN->forwardProb(inputLayer, outputLayer, false);
        }
        else{
            GDNN3L->forwardProb(inputLayer, outputLayer, false);
        }
    }


	//set init pid Param
	double pidParam[12]={0.5,0.5,0.5,
					  0.5,0.5,0.5,
                         0.5,0.5,0.5,
                         0.5,0.5,0.5};
	//combineSendPidParam(pidParam);



    while(true){
		cout << "Please enter a valid command:\n>";
		getline(cin, consigne);
		stringstream input_stringstream(consigne);
		cout << "Received command: " << consigne << endl << endl;
		double *vError[4];
		for(int idx=0;idx<4;idx++){
			vError[idx]=new double[3];
			for(int idy=0; idy<3;idy++){
				vError[idx][idy]=0;
			}

		}

		//if is not ending command

		getline(input_stringstream,parsed,' ');
		//drive with CONSTANT SPEED
		if(parsed[0]=='d'){

			getline(input_stringstream,parsed,' ');
			allerA = std::stof(parsed);
			getline(input_stringstream,parsed,' ');
			allerV = std::stof(parsed);
			getline(input_stringstream,parsed,' ');
			rotationV = std::stof(parsed);
			cout << "Received angle: " << allerA << " "<<allerV<<" "<< rotationV  << endl << endl;

                //Start driving
                combinedDrive(allerA, allerV,rotationV,inputLayer, 1000);
                //train GDNN

            if(DRNNType[0]=='s'){
                GDNNS->forwardProb(inputLayer,outputLayer,false);
            }
            else{
                if(DRNNLayer[0]=='s') {
                    GDNN->forwardProb(inputLayer, outputLayer, false);
                }
                else{
                    GDNN3L->forwardProb(inputLayer, outputLayer, false);
                }
            }

            //set PID param
            if(enaRNN){
                combineSendPidParam(outputLayer);
            }
            else{
                combineSendPidParam(pidParam);
            }


                for(int idx=0;idx<10;idx++){
                    //get three previous error
                    for(int idm=0;idm<4;idm++){
                        uartGetError(idm+1, vError[idm]);
                        cout<<"motor "<<idm+1<<" error current "<<vError[idm][0]<<endl;
                    }

                    if(DRNNType[0]=='s'){
                        GDNNS->backwardprob(inputLayer,vError,1);
                        GDNNS->forwardProb(inputLayer,outputLayer,false);
                    }
                    else{
                        if(DRNNLayer[0]=='s') {
                            GDNN->backwardprob(inputLayer, vError, 1);
                            GDNN->forwardProb(inputLayer, outputLayer, false);
                        }
                        else{
                            GDNN3L->backwardprob(inputLayer, vError, 1);
                            GDNN3L->forwardProb(inputLayer, outputLayer, false);
                        }
                    }


                    //set PID param
                    if(enaRNN){
                        combineSendPidParam(outputLayer);
                    }
                    else{
                        combineSendPidParam(pidParam);
                    }

                }

		}
		//drive along determined path
		if(parsed[0]=='p'){
			//get filename
			getline(input_stringstream,parsed,' ');

			std::ifstream infile(parsed);
			std::string line;
			while (std::getline(infile, line))
			{
				stringstream file_stringstream(line);
				getline(file_stringstream,parsed,' ');
				allerA = std::stof(parsed);
				getline(file_stringstream,parsed,' ');
				allerV = std::stof(parsed);
				getline(file_stringstream,parsed,' ');
				rotationV = std::stof(parsed);
				getline(file_stringstream,parsed,' ');
				execTime = std::stof(parsed);

				cout << "Exec CMD: " << allerA << " "<<allerV<<" "<< rotationV  <<" "<< execTime << endl;

				// drive consigne: Aller_angle, vitesseV, rotationV, time.
				getVelocity(allerA, allerV,rotationV,inputLayer);


				//train GDNN
                if(DRNNType[0]=='s'){
                    GDNNS->forwardProb(inputLayer,outputLayer,false);
                }
                else{
                    if(DRNNLayer[0]=='s') {
                        GDNN->forwardProb(inputLayer, outputLayer, false);
                    }
                    else{
                        GDNN3L->forwardProb(inputLayer, outputLayer, false);
                    }
                }
				//set PID param
                if(enaRNN){
                    combineSendPidParam(outputLayer);
                }
                else{
                    combineSendPidParam(pidParam);
                }

				//Start driving
				combinedDrive(allerA, allerV,rotationV,inputLayer, (int)(execTime*execTFactor));

                //only train 10 times
                int idx=0;
				while(idx<(execTime*iterFactor)){
					//get three previous error
                    gettimeofday(&end, NULL);
                    seconds  = end.tv_sec  - start.tv_sec;
                    useconds = end.tv_usec - start.tv_usec;

                    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
                    //train once 10 ms
                    if (mtime-lastExecT>=300){
                        lastExecT=mtime;
                        cout<<"now time "<<mtime<<endl;
                        errorFile<<mtime<<" ";
                        errorFile<<allerV<<" ";
                        for(int idm=0;idm<4;idm++){
                            uartGetError(idm+1, vError[idm]);
                            cout<<"motor "<<idm+1<<" error current "<<vError[idm][0]<<endl;
                            errorFile<<inputLayer[idm]<<" "<<vError[idm][0]<<" ";
                        }
                        errorFile<<endl;

                        if(DRNNType[0]=='s'){
                            GDNNS->backwardprob(inputLayer,vError,1);
                            GDNNS->forwardProb(inputLayer,outputLayer,false);
                        }
                        else{
                            if(DRNNLayer[0]=='s') {
                                GDNN->backwardprob(inputLayer, vError, 1);
                                GDNN->forwardProb(inputLayer, outputLayer, false);
                            }
                            else{
                                GDNN3L->backwardprob(inputLayer, vError, 1);
                                GDNN3L->forwardProb(inputLayer, outputLayer, false);
                            }
                        }
                        //set PID param
                        if(enaRNN){
                            combineSendPidParam(outputLayer);
                        }
                        else{
                            combineSendPidParam(pidParam);
                        }
                        idx++;
                    }
				}

			}
			errorFile.close();
		}

		if(parsed[0]=='r'){

            uartGetError(1,vError[0]);
            uartGetError(2,vError[1]);
            uartGetError(3,vError[2]);
            uartGetError(4,vError[3]);

        }

        if(parsed[0]=='i'){

            uartGetInt(1);
            uartGetInt(2);
            uartGetInt(3);
            uartGetInt(4);

        }

		if(parsed[0]=='e'){
			function_exit();
            if(enaRNN){
                if(DRNNType[0]=='s'){
                    GDNNS->saveParameterToFile(envName);
                }
                else{
                    if(DRNNLayer[0]=='s') {
                        GDNN->saveParameterToFile(envName);
                    }
                    else{
                        GDNN3L->saveParameterToFile(envName);
                    }
                }
            }

			return 0;

		}

    }


	return 0;
}
