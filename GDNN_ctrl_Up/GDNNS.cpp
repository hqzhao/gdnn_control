//
// Created by hanqing_zhao on 2019/3/29.
//

#include "GDNNS.h"
#define N  999

void reseauGDNNS::initiateGDNN(std::string envFileName) {
    int numNodes = 22;
    //init bias to 0
    bias1 = new double[numNodes];
    nodesOutLast1 = new double[numNodes];
    bias2 = new double[numNodes];
    nodesOutLast2 = new double[numNodes];
    bias3 = new double[numNodes];
    nodesOutLast3 = new double[numNodes];
    bias4 = new double[numNodes];
    nodesOutLast4 = new double[numNodes];


    for(int idx=0; idx<numNodes;idx++){
        bias1[idx]=0;
        nodesOutLast1[idx]=0;
        bias2[idx]=0;
        nodesOutLast2[idx]=0;
        bias3[idx]=0;
        nodesOutLast3[idx]=0;
        bias4[idx]=0;
        nodesOutLast4[idx]=0;

    }
    //init weights
    weight1 = new double*[numNodes];
    weight2 = new double*[numNodes];
    weight3 = new double*[numNodes];
    weight4 = new double*[numNodes];

    for(int idx=0;idx<numNodes;idx++){
        weight1[idx]=new double[numNodes];
        weight2[idx]=new double[numNodes];
        weight3[idx]=new double[numNodes];
        weight4[idx]=new double[numNodes];

    }

    //try to read parameters from configure file
    int succ=readParameterFromFile(envFileName);

    //init from file failed, randomly init
    if(succ==0){
        // nodes 0 to 3, three input nodes, 4 to 18, hidden nodes, 19 to 30 output nodes
        for(int idx=0; idx<1;idx++){
            //forward connection from input to first layer
            for(int idy=1; idy<16;idy++) {
                weight1[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight2[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight3[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight4[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }
        //delayed propagation backwards
        for(int idx=1; idx<16;idx++){
            //forward connection from input to first layer
            for(int idy=0; idy<1;idy++) {
                weight1[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight2[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight3[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight4[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }
        //output propagation forwards
        for(int idx=1; idx<16;idx++){
            //forward connection from input to first layer
            for(int idy=16; idy<19;idy++) {
                weight1[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight2[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight3[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight4[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }

        //output propagation forwards 2nd layer
        for(int idx=16; idx<19;idx++){
            //forward connection from input to first layer
            for(int idy=19; idy<22;idy++) {
                weight1[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight2[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight3[idx][idy] = rand() % (N + 1) / (double) (N + 1);
                weight4[idx][idy] = rand() % (N + 1) / (double) (N + 1);
            }
        }
    }


    //return 0;

};
//forward probagation output calculation
int reseauGDNNS::forwardProb(double *inputLayer, double *outputLayer,bool changeOutLast) {
    //intermediary output
    double nodeOut1[22];
    double nodeOut2[22];
    double nodeOut3[22];
    double nodeOut4[22];

    for(int idx=0; idx<22; idx++){
        nodeOut1[idx]=0;
        nodeOut2[idx]=0;
        nodeOut3[idx]=0;
        nodeOut4[idx]=0;
    }
    //output of four input nodes
    for(int idx=0;idx<1;idx++){
        for(int idy=1;idy<16;idy++){
            nodeOut1[idx]+=weight1[idy][idx]*nodesOutLast1[idy];
            nodeOut2[idx]+=weight2[idy][idx]*nodesOutLast2[idy];
            nodeOut3[idx]+=weight3[idy][idx]*nodesOutLast3[idy];
            nodeOut4[idx]+=weight4[idy][idx]*nodesOutLast4[idy];
        }
        nodeOut1[idx]+=inputLayer[idx];
        nodeOut2[idx]+=inputLayer[idx+1];
        nodeOut3[idx]+=inputLayer[idx+2];
        nodeOut4[idx]+=inputLayer[idx+3];
        nodeOut1[idx]=tanh(nodeOut1[idx]+bias1[idx]);
        nodeOut2[idx]=tanh(nodeOut2[idx]+bias2[idx]);
        nodeOut3[idx]=tanh(nodeOut3[idx]+bias3[idx]);
        nodeOut4[idx]=tanh(nodeOut4[idx]+bias4[idx]);
    }

    //output for 15 hidden nodes
    for(int idx=1; idx<16;idx++){
        for(int idy=0;idy<1;idy++){
            nodeOut1[idx]+=weight1[idy][idx]*nodeOut1[idy];
            nodeOut2[idx]+=weight2[idy][idx]*nodeOut2[idy];
            nodeOut3[idx]+=weight3[idy][idx]*nodeOut3[idy];
            nodeOut4[idx]+=weight4[idy][idx]*nodeOut4[idy];
        }
        nodeOut1[idx]=tanh(nodeOut1[idx]+bias1[idx]);
        nodeOut2[idx]=tanh(nodeOut2[idx]+bias2[idx]);
        nodeOut3[idx]=tanh(nodeOut3[idx]+bias3[idx]);
        nodeOut4[idx]=tanh(nodeOut4[idx]+bias4[idx]);
    }
    //output for 2nd layer hidden nodes
    for(int idx=16;idx<19;idx++){
        for(int idy=1;idy<16;idy++){
            nodeOut1[idx]+=weight1[idy][idx]*nodeOut1[idy];
            nodeOut2[idx]+=weight2[idy][idx]*nodeOut2[idy];
            nodeOut3[idx]+=weight3[idy][idx]*nodeOut3[idy];
            nodeOut4[idx]+=weight4[idy][idx]*nodeOut4[idy];
        }
        nodeOut1[idx]=sigmoid(nodeOut1[idx]+bias1[idx]);
        nodeOut2[idx]=sigmoid(nodeOut2[idx]+bias2[idx]);
        nodeOut3[idx]=sigmoid(nodeOut3[idx]+bias3[idx]);
        nodeOut4[idx]=sigmoid(nodeOut4[idx]+bias4[idx]);

    }

    //output for 12 output nodes
    for(int idx=19;idx<22;idx++){
        for(int idy=16;idy<19;idy++){
            nodeOut1[idx]+=weight1[idy][idx]*nodeOut1[idy];
            nodeOut2[idx]+=weight2[idy][idx]*nodeOut2[idy];
            nodeOut3[idx]+=weight3[idy][idx]*nodeOut3[idy];
            nodeOut4[idx]+=weight4[idy][idx]*nodeOut4[idy];
        }
        nodeOut1[idx]=sigmoid(nodeOut1[idx]+bias1[idx]);
        nodeOut2[idx]=sigmoid(nodeOut2[idx]+bias2[idx]);
        nodeOut3[idx]=sigmoid(nodeOut3[idx]+bias3[idx]);
        nodeOut4[idx]=sigmoid(nodeOut4[idx]+bias4[idx]);
        outputLayer[idx-19]=nodeOut1[idx];
        outputLayer[idx-19+3]=nodeOut2[idx];
        outputLayer[idx-19+6]=nodeOut3[idx];
        outputLayer[idx-19+9]=nodeOut4[idx];

    }

    //change last output record if
    if(changeOutLast){
        for(int idx=0;idx<22;idx++){
            nodesOutLast1[idx]=nodeOut1[idx];
            nodesOutLast2[idx]=nodeOut2[idx];
            nodesOutLast3[idx]=nodeOut3[idx];
            nodesOutLast4[idx]=nodeOut4[idx];
        }
    }
    return 0;
}

double reseauGDNNS::sigmoid(double x) {
    double exp_value;
    double return_value;

    /*** Exponential calculation ***/
    exp_value = exp((double) -x);

    /*** Final sigmoid value ***/
    return_value = 1 / (1 + exp_value);

    return return_value;
}

//BP training
int reseauGDNNS::backwardprob(double *inputLayer, double **vError, double learningRate) {
    double newWeight1[22][22],newWeight2[22][22],newWeight3[22][22],newWeight4[22][22];
    double dervEop[12], origOut[12], deltaOut[12], dervWoe[12];

    //derv1 derivative of network output and weight
    //derv2 derivative of error func and network output
    double derv1, derv2;

    //calculate derivation of error func to each output
    for(int idx=0;idx<12;idx++){
        dervEop[idx]=-(vError[idx/3][0]);
        //if for KP
        if(idx%3==0) dervEop[idx]*=(vError[idx/3][0]-vError[idx/3][1]);
        //if for kI
        if(idx%3==1) dervEop[idx]*=(vError[idx/3][0]);
        //if for kD
        if(idx%3==2) dervEop[idx]*=(vError[idx/3][0]-2*vError[idx/3][1]+vError[idx/3][2]);
    }

    //calculate original output
    forwardProb(inputLayer, origOut, false);

    //calculate derivation of each output to each weight and update weight
    for(int idx=0;idx<22;idx++){
        for(int idy=0;idy<22;idy++){
            newWeight1[idy][idx]=weight1[idy][idx];


            newWeight2[idy][idx]=weight2[idy][idx];


            newWeight3[idy][idx]=weight3[idy][idx];


            newWeight4[idy][idx]=weight4[idy][idx];


            for(int idz=0; idz<12; idz++){
                if(idz/3==0){
                    weight1[idy][idx]-=1e-7;
                    forwardProb(inputLayer, deltaOut, false);
                    weight1[idy][idx]+=1e-7;
                    dervWoe[idz]=(origOut[idz]-deltaOut[idz])/(1e-7);
                    newWeight1[idy][idx]-=learningRate*dervWoe[idz]*dervEop[idz];
                }
                if(idz/3==1){
                    weight2[idy][idx]-=1e-7;
                    forwardProb(inputLayer, deltaOut, false);
                    weight2[idy][idx]+=1e-7;
                    dervWoe[idz]=(origOut[idz]-deltaOut[idz])/(1e-7);
                    newWeight2[idy][idx]-=learningRate*dervWoe[idz]*dervEop[idz];
                }
                if(idz/3==2){
                    weight3[idy][idx]-=1e-7;
                    forwardProb(inputLayer, deltaOut, false);
                    weight3[idy][idx]+=1e-7;
                    dervWoe[idz]=(origOut[idz]-deltaOut[idz])/(1e-7);
                    newWeight3[idy][idx]-=learningRate*dervWoe[idz]*dervEop[idz];
                }
                if(idz/3==3){
                    weight4[idy][idx]-=1e-7;
                    forwardProb(inputLayer, deltaOut, false);
                    weight4[idy][idx]+=1e-7;
                    dervWoe[idz]=(origOut[idz]-deltaOut[idz])/(1e-7);
                    newWeight4[idy][idx]-=learningRate*dervWoe[idz]*dervEop[idz];
                }
            }

        }
    }


    // renew original weights
    for(int idx=0;idx<22;idx++){
        for(int idy=0;idy<22;idy++){
            weight1[idy][idx]=newWeight1[idy][idx];
            weight2[idy][idx]=newWeight2[idy][idx];
            weight3[idy][idx]=newWeight3[idy][idx];
            weight4[idy][idx]=newWeight4[idy][idx];
        }
    }



}


int reseauGDNNS::readParameterFromFile(std::string envFileName) {
    std::ifstream infile(envFileName);
    //if no file found
    if(!infile){
        std::cout<<"DRNN parameter file not found, randomly init "<<std::endl;
        return 0;
    }
    std::string line;
    std::getline(infile, line);
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            weight1[idx][idy]=std::stof(line);
            std::getline(infile, line);
        }

    }
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            weight2[idx][idy]=std::stof(line);
            std::getline(infile, line);
        }

    }
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            weight3[idx][idy]=std::stof(line);
            std::getline(infile, line);
        }

    }
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            weight4[idx][idy]=std::stof(line);
            std::getline(infile, line);
        }

    }
    infile.close();
    std::cout<<"DRNN parameter read successfully "<<std::endl;
    return 1;

}

int reseauGDNNS::saveParameterToFile(std::string envFileName) {
    std::ofstream outfile;
    outfile.open(envFileName);
    outfile<< std::setprecision(8) << std::fixed;
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            outfile << weight1[idx][idy] << std::endl;
        }

    }
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            outfile << weight2[idx][idy] << std::endl;
        }

    }
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            outfile << weight3[idx][idy] << std::endl;
        }

    }
    for(int idx=0; idx<22; idx++){
        for(int idy=0; idy<22; idy++){
            outfile << weight4[idx][idy] << std::endl;
        }

    }

    outfile.close();
}