//
// Created by hanqing_zhao on 2019/2/22.
//
//
#ifndef DEBUGGL_GDNN_H
#define DEBUGGL_GDNN_H
#include <cstdlib>
#include <math.h>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>

class reseauGDNN{
    double **weight;
    double *bias;
    double *nodesOutLast;
public:
    void initiateGDNN(std::string envFileName);
    int forwardProb(double* inputLayer, double* outputLayer,bool changeOutLast=true);
    double sigmoid(double x);
    int backwardprob(double* inputLayer, double** vError, double learningRate);
    int saveParameterToFile(std::string envFileName);
    int readParameterFromFile(std::string envFileName);


};
#endif //DEBUGGL_GDNN_H
